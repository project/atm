drupal-module
=============

[![Build Status](https://travis-ci.org/AdTechMedia/drupal-module.svg?branch=master)](https://travis-ci.org/AdTechMedia/drupal-module)
[![Test Coverage](https://codeclimate.com/repos/57dff2b4f01b5b648b0042b0/badges/aed49615ace44e12bda8/coverage.svg)](https://codeclimate.com/repos/57dff2b4f01b5b648b0042b0/coverage)

Drupal Module for AdTechMedia.io

Ad Tech Media (aka ATM) is an Innovative B2B Solution to Monetize Content through Micropayments

